import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;

import { rtdbPlugin } from "vuefire";
import { typedVuefirePlugin } from "@zoomsense/zoomsense-firebase";
// import { Quasar } from "quasar";
// import quasarUserOptions from "./quasar-user-options";
Vue.use(rtdbPlugin);
Vue.use(typedVuefirePlugin);
// Vue.use(Quasar, quasarUserOptions);

import upperFirst from "lodash/upperFirst";
import camelCase from "lodash/camelCase";
// import "./quasar";

import { Quasar } from "quasar";
import quasarUserOptions from "./quasar-user-options";
Vue.use(Quasar, quasarUserOptions);

import Plugin from "@quasar/quasar-ui-qmediaplayer";
import "@quasar/quasar-ui-qmediaplayer/dist/index.css";

Vue.use(Plugin as any);

console.log(`Loading plugin from: ${process.env.VUE_APP_COMPONENTS}`);
// console.log(process.env.VUE_APP_SEQUENCEID);

// const Dashboard = () =>
// import(`${process.env.VUE_APP_COMPONENTS}/Dashboard.vue`);

// Vue.component("Dashboard", Dashboard);

const requireComponent = require.context(
  // The relative path of the components folder
  "./components",
  // Whether or not to look in subfolders
  false,
  // The regular expression used to match base component filenames
  /[A-Z]\w+\.(vue|js)$/
);

// console.log(requireComponent.keys());

requireComponent.keys().forEach((fileName) => {
  // Get component config
  const componentConfig = requireComponent(fileName);

  // Get PascalCase name of component
  const componentName = upperFirst(
    camelCase(
      // Gets the file name regardless of folder depth
      fileName
        .split("/")
        .pop()!
        .replace(/\.\w+$/, "")
    )
  );

  console.log(`Registering ${componentName}`);

  // Register component globally
  Vue.component(
    componentName,
    // Look for the component options on `.default`, which will
    // exist if the component was exported with `export default`,
    // otherwise fall back to module's root.
    componentConfig.default || componentConfig
  );
});

new Vue({
  render: (h) => h(App),
}).$mount("#app");
