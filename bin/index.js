#!/usr/bin/env node

let argv = require("yargs/yargs")(process.argv.slice(2))
  .scriptName("zoomsense-plugin-harness")
  .usage("$0 [args] [directory]")
  .command("$0 [args] [directory]", "Start plugin development harness", (yargs) => {
    yargs.positional("directory", {
      describe: "The path to the plugin's components directory (the one containing .vue files).  If omitted, defaults to the current directory.",
    });
  })
  .option("k", {
    alias: "apikey",
    type: "string",
    describe: "ZoomSense Developer APIKEY",
    required: true,
  })
  .option("e", {
    alias: "email",
    type: "string",
    describe: "ZoomSense Developer Email",
    required: true,
  })
  .option("P", {
    alias: "password",
    type: "string",
    describe: "ZoomSense Developer Password",
    required: true,
  })
  .option("m", {
    alias: "meeting",
    type: "string",
    describe: "ZoomSense meeting id",
    required: true,
  })
  .option("s", {
    alias: "sequence",
    type: "string",
    describe: "ZoomSense sequence id",
  })
  .option("b", {
    alias: "database",
    type: "string",
    describe: "Firebase RTDB URL (e.g. http://localhost:9000/?ns=zoomsense-plugin-default-rtdb)",
  })
  .option("f", {
    alias: "functions",
    type: "string",
    describe: "Firebase functions URL (e.g. http://localhost:5001)",
  })
  .option("p", {
    alias: "project",
    type: "string",
    describe: "Firebase Project ID",
  })
  .demandCommand(1)
  .help().argv;

// console.log(argv);

// console.log(__dirname);
const path = require("path");
const fs = require("fs");

const { spawn } = require("child_process");

process.env.VUE_APP_COMPONENTS = path.resolve(argv.directory || ".");
process.env.VUE_APP_MEETINGID = argv.meeting;
process.env.VUE_APP_SEQUENCEID = argv.sequence || "test";
process.env.VUE_APP_EMAIL = argv.email;
process.env.VUE_APP_PASSWORD = argv.password;
process.env.VUE_APP_APIKEY = argv.apikey;

if (argv.database) process.env.VUE_APP_DATABASEURL = argv.database;
if (argv.functions) process.env.VUE_APP_FUNCTIONSURL = argv.functions;
if (argv.project) process.env.VUE_APP_PROJECTID = argv.project;

const componentsSource = process.env.VUE_APP_COMPONENTS;
const componentsDestination = path.resolve(__dirname, "..", "src", "components");
const nodeModulesSource = path.resolve(process.env.VUE_APP_COMPONENTS, "..", "..", "node_modules");
const nodeModulesDestination = path.resolve(__dirname, "..", "src", "node_modules");

try {
  fs.unlinkSync(componentsDestination);
} catch (e) {
  //do nothing
}

try {
  fs.unlinkSync(nodeModulesDestination);
} catch (e) {
  //do nothing
}

const isWindows = require("os").platform() === "win32";

if (isWindows) {
  const fsExtra = require("fs-extra");

  console.log("Windows setup detected: creating junction to node_modules.");
  fs.symlinkSync(nodeModulesSource, nodeModulesDestination, "junction");

  console.log(
    "\tComponents will be copied over on file changes.\n" +
      "\tAdding new files to Components, or changes to node_modules, will still require a restart."
  );

  fsExtra.copySync(componentsSource, componentsDestination, { dereference: true });

  // Store the filenames currently in componentsSource, to avoid copying temporary backup files created by IDEs when saving.
  const originalFiles = fs.readdirSync(componentsSource).reduce((original, file) => {
    original[file] = true;
    return original;
  }, {});

  fs.watch(componentsSource, { recursive: true }, (eventType, fileName) => {
    if (eventType === "change" && originalFiles[fileName]) {
      fs.copyFileSync(path.join(componentsSource, fileName), path.join(componentsDestination, fileName));
    }
  });
} else {
  fs.symlinkSync(componentsSource, componentsDestination);

  fs.symlinkSync(nodeModulesSource, nodeModulesDestination);
}

spawn("npx", ["vue-cli-service", "serve"], {
  stdio: "inherit",
  cwd: path.resolve(__dirname, ".."),
  shell: isWindows,
});
