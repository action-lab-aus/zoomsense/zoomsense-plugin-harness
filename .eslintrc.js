module.exports = {
  root: true,
  env: {
    es6: true,
    node: true,
  },
  parser: "vue-eslint-parser",
  plugins: ["@typescript-eslint"],
  extends: ["eslint:recommended", "plugin:vue/base", "plugin:@typescript-eslint/base"],
  parserOptions: {
    "parser": "@typescript-eslint/parser"
  },
  rules: {
    quotes: ["error", "double"],
    "quote-props": "off",
    "object-curly-spacing": ["error", "always"],
  },
};
