# ZoomSense Plugin Harness

ZoomSense plugins are small units of Vue and Firebase Function code that is compiled as a library and downloaded by the ZoomSense infrastructure at runtime.

The development of these plugins requires a test harness that provides the expected dependencies and data that will be available in the final deployment environment.

Developers creating plugins that provide a UI (Dashboard.vue, Overlay.vue, Room.vue) should use this tool to develop their plugins before deployment to ZoomSense.

# Installation

Install the harness globally using npm, this will allow you to use it inside any plugin repository.

```
npm install -g @zoomsense/zoomsense-plugin-harness
```

## Installation on Windows Environments

Unlike previous versions of the harness, the latest version works on Windows without using WSL.  In fact, it should not
be run under WSL, because file changes will not be detected.  Run the harness in powershell instead.

Note that while changes to your dashboard and overlay components will be automatically detected and hot-reloaded by the
harness, two sorts of changes will not be detected, and will require the harness to be restarted:

* Adding new files to the Components folder.
* Adding new dependencies to package.json, installed to `node_modules`.

# Usage

From a terminal (usually easiest from inside your plugin development repo), run the following command (please note that the password of the admin account is using an uppercase P):

```
zoomsense-plugin-harness -m <meetingid in the zoomsense-plugin firebase> -e <email of admin account> -P <password of admin account> -k <firebase API key> -s (optional) <sequenceid> [directory]
```

The optional [directory] argument should indicate the directory containing the plugin's components.  It defaults to the
current working directory if not provided.

Example:

Go to the `app/src/components` directory in your plugins repo, and run `zoomsense-plugin-harness -m <meetingid> -e <email> -p <password> -k <key>`

Alternatively, from the top level directory of your plugins repo, you could run `zoomsense-plugin-harness -m <meetingid> -e <email> -p <password> -k <key> ./app/src/components`
